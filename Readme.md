
https://help.dropbox.com/installs-integrations/desktop/system-requirements#linux

https://www.dropbox.com/install-linux

https://help.dropbox.com/installs-integrations/desktop/linux-commands#commands

# Linking dropbox account

When starting for the first time (or with an empty settings volume) the dropbox client is not
linked to any account. In order to link to dropbox client, look into the container logs for the following lines:
```
...
This computer isn't linked to any Dropbox account...
Please visit https://www.dropbox.com/cli_link_nonce?nonce=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx to link this device.
...
```

Open the link specified by `https://www.dropbox.com/cli_link_nonce?nonce=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx`. This page
will ask you to login with a dropbox account, this is the account that will get associated with this dropbox client.
We use a robot account for this.

## Re-linking

If at any point you want to relink the dropbox client to a different account, you can do the following steps:

0. Backup the settings volume.
``` 
#Create backup volume
docker volume create ce_archive_dropbox-settings-backup

#Mount settings and backup volume and create the backup
docker run -ti --rm \
    -v ce_archive_dropbox-settings:/input \
    -v ce_archive_dropbox-settings-backup:/output \
    alpine \
        sh -c 'apk update && \
                apk add rsync && \
                rsync -avpP /input/ /output/'
                
                
#Create backup volume
docker volume create ce_archive_dropbox-backup

#Mount settings and backup volume and create the backup
docker run -ti --rm \
    -v ce_archive_dropbox:/input \
    -v ce_archive_dropbox-backup:/output \
    alpine \
        sh -c 'apk update && \
                apk add rsync && \
                rsync -avpP /input/ /output/'                    
 ```
1. Clear the settings volume
```
docker run -ti --rm \
    -v ce_archive_dropbox-settings:/input 
    alpine \
        sh -c 'rm -rf /input/*'
```
3. Restart the dropbox client container

# Dropbox filesystem changes

Dropbox has been changing its stance on what filesystems to support. At some point they announced:
```
The supported file systems are Ext4 filesystem on Linux, NTFS for Windows, and HFS+ or APFS for Mac.
```
but later came back from it and added support again for some of the earlier supported filesystems:
``` 
Add support for zfs (on 64-bit systems only), eCryptFS, xfs (on 64-bit systems only), and btrfs filesystems in Linux.
```
Reference: [Dropbox walks back its own decision; brings back support for ZFS, XFS, Btrfs, and eCryptFS on Linux](https://hub.packtpub.com/dropbox-walks-back-its-own-decision-brings-back-support-for-zfs-xfs-btrfs-and-ecryptfs-on-linux/)

For our docker setup this is causing issues, since inside a docker container the filesystem we are using is "overlay",
which is not supported by docker. The client will issue an alert regarding an unsupported file system.