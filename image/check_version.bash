#!/usr/bin/env bash

# Get version from url
url="https://www.dropbox.com/download?plat=lnx.x86_64"
location=$(curl -v -X HEAD "${url}" 2>&1 | grep "< location")

regex=".*x86_64-(.+).tar.gz"
[[ "${location}" =~ $regex ]]

if [ "${BASH_REMATCH[1]}" == "" ]; then
  echo "Failed to fetch version from url=${url}"
  exit
fi

version_available="${BASH_REMATCH[1]}"

# Get version from process
process=$(ps aux | grep dropbox | grep -v "grep dropbox")
if [ "${process}" == "" ]; then
  echo "No running dropbox process found"
  exit
fi

regex=".*x86_64-(.+)/dropbox"
[[ "${process}" =~ $regex ]]
if [ "${BASH_REMATCH[1]}" == "" ]; then
  echo "Failed to fetch version from process"
  exit
fi

version_running="${BASH_REMATCH[1]}"

# Print result

if [ "${version_running}" != "${version_available}" ]; then
  echo "New verion available: running=${version_running}, available=${version_available}"
else
  echo "Running latest verion (${version_running})"
fi