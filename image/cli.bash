#!/usr/bin/env bash

_CACHE_DIR=${CACHE_DIR:-"/home/user/Dropbox/.dropbox.cache"}

MODE=""

MODE_CHECK="MODE_CHECK"
MODE_CLEANUP="MODE_CLEANUP"
MODE_VERSION="MODE_VERSION"
MODE_HELP="MODE_HELP"
MODE_HELP_UNKOWN="MODE_HELP_UNKOWN"

version() {
    cat "/home/user/.dropbox-dist/VERSION"
}

check() {
    CURRENT_VERSION=$(cat "/home/user/.dropbox-dist/VERSION" | tr -cd "[:print:]")
    AVAILABLE_VERSION=$(curl -sI https://www.dropbox.com/download\?plat\=lnx.x86_64 | grep location | pcregrep -o1 -i '.*/dropbox-lnx.x86_64-(.+).tar.gz'  | tr -cd "[:print:]")

    if [ "${CURRENT_VERSION}" != "${AVAILABLE_VERSION}" ]; then
        echo "A new version is available: ${AVAILABLE_VERSION} (current version = ${CURRENT_VERSION})"
    else
        echo "Current version (${CURRENT_VERSION}) is the latest version"
    fi
}

cleanup() {
    if [ ! -d "${_CACHE_DIR}" ]; then
        echo "Dropbox cache directory does not exist"
    else
        NUM_FILES=$(find . "${_CACHE_DIR}" | wc -l)
        SIZE_FILE=$(du -sh "${_CACHE_DIR}" | cut -d$'\t' -f1)

        MSG="$(date) cleanup, #files in cache=${NUM_FILES}, cache size=${SIZE_FILE}"

        echo "${MSG}"
        echo "${MSG}" >> /home/user/dropbox_cleanup.log
        rm -rf "${_CACHE_DIR}"
    fi
}

help() {
    echo "Usage: cli <options>"
    echo ""
    echo "Options:"
    echo "      --check         Check if there is a new dropbox daemon version"
    echo "      --cleanup       Cleanup dropbox cache directory"
    echo "  -v, --version       Print dropbox daemon version"
    echo "  -h, --help          Show this help"
}

main() {
    #
    # Process script arguments
    #
    while [[ $# -gt 0 ]]
    do
    key="$1"
    case $key in
        --check)
            MODE="${MODE_CHECK}"
            ;;
        --cleanup)
            MODE="${MODE_CLEANUP}"
            ;;
        -v|--version)
            MODE="${MODE_VERSION}"
            ;;
        -h|--help)
            MODE="${MODE_HELP}"
            ;;
        *)
            echo "Unkown option: $key"
            MODE=="${MODE_HELP_UNKOWN}"
            ;;
    esac
    shift # past argument or value
    done

    if [ "${MODE}" == "${MODE_VERSION}" ]; then
        version
    elif [ "${MODE}" == "${MODE_CHECK}" ]; then
        check
    elif [ "${MODE}" == "${MODE_CLEANUP}" ]; then
        cleanup
    elif [ "${MODE}" == "${MODE_HELP}" ]; then
        help
    else
        help
    fi

}

main $@